import requests
import json
import time
import dpath.util as dpath
from pathos.multiprocessing import ProcessingPool
from flask import Flask, render_template
from threading import Thread
import argparse

last_input = None

# --------------------------------- CONSTANTS ----------------------------------
ROOT = 'http://partners.api.skyscanner.net/apiservices/'
KEY  = 'ha735495689835252807543252794422'
UID  = 'i028f4656-cbb1-43fb-8952-5d1f0107a39e'

URLS = {
    'locales':    'reference/v1.0/locales',
    'markets':    'reference/v1.0/countries/%(locale)s',
    'currencies': 'reference/v1.0/currencies',
    'places':     '/autosuggest/v1.0/%(market)s/%(currency)s/%(locale)s?query=%(query)s',
    'quotes':     'browsequotes/v1.0/%(country)s/%(currency)s/%(locale)s/%(src)s/%(dst)s/%(date_out)s/%(date_in)s',
    'session':    '/pricing/v1.0',
}

BODY = 'cabinclass=Economy&country=%(country)s&currency=%(currency)s&locale=%(locale)s&locationSchema=sky&originplace=%(src)s&destinationplace=%(dst)s&outbounddate=%(date_out)s&inbounddate=%(date_in)s&adults=1&children=0&infants=0&apikey=' + KEY

HEADERS = {
    'Content-Type': "application/x-www-form-urlencoded",
    'cache-control': "no-cache",
}


class Request:

    def __init__(self, req_id: int, src: str, dst: str, date_out: str):
            """ object that represents the state of every request, in table we build the results for the request
            """
            self.table = []  # [(flag, country, price)]
            self.poll_id = req_id
            self.src = src
            self.dst = dst
            self.date_out = date_out


reqs = {
    0: Request(0, 'src', 'dst', 'date_out')
}


app = Flask(__name__)


# ------------------------------------------------------------------------------
# ------------------------------- API REQUESTS ---------------------------------
# ------------------------------------------------------------------------------
def to_url(s: str) -> str:
    root = ROOT.rstrip('/')
    s = s.strip('/')
    arg_sep = '&' if '?' in s else '?'
    return f'{root}/{s}{arg_sep}apiKey={KEY}'
    #return root+'/'+s+arg_sep+'apikey='+KEY


def decode_response(part_url: str, *, encoding='utf-8', **kwargs) -> dict:
    part_url = part_url % kwargs
    url      = to_url(part_url)
    bstr     = requests.get(url).content
    return json.loads(bstr.decode(encoding))


def post_response(part_url=URLS['session'], body=BODY, headers=HEADERS, 
                  **kwargs) -> dict:
    body = body % kwargs
    root     = ROOT.rstrip('/')
    part_url = part_url.strip('/')
    url      = f'{root}/{part_url}'
    #url      = root+'/'+part_url
    return requests.post(url, data=body, headers=headers).headers


# ------------------------------------------------------------------------------
# -------------------------------- UTILITIES -----------------------------------
# ------------------------------------------------------------------------------
def pprint_reqs(sort_keys=True) -> str:
    reqs_ = {rid : len(r.table) for rid, r in reqs.items()}
    print(json.dumps(reqs_, indent=4, sort_keys=sort_keys))

def extract(path: str):
    def decorator(func):
        def wrapper(*args, **kwargs):
            r = func(*args, **kwargs)
            return dpath.values(r, path)
        return wrapper
    return decorator

def duration(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        try:
            r  = func(*args, **kwargs)
        finally:
            dt = time.time() - start_time
            print(f'TOTAL TIME {func.__name__}: {dt} seconds')
            #print('TOTAL TIME '+func.__name__+': '+dt+' seconds')
        return r 
    return wrapper


# ------------------------------------------------------------------------------
# ----------------------------- MARKETS SWEEPER --------------------------------
# ------------------------------------------------------------------------------
class MarketsSweeper:

    def __init__(self, src: str, dst: str,
                 date_out: str,  date_in='',
                 currency='GBP', locale='en-GB', init_market='ES'):
        """
        Args:
            src      (str): outbound airport/ origin
            dst      (str): inbound airport / destination
            date_out (str): 'yyyy-mm-dd'
            date_in  (str): 'yyyy-mm-dd' or by def '' (oneway trip)
        """
        self.details = {
            'date_out': date_out,
            'date_in':  date_in,
            'currency': currency,
            'locale':   locale,
            'market':   init_market
        }
        # get src and dst codes and update self.details
        src, = self.get_place(src)
        dst, = self.get_place(dst)
        print(f'FROM: {src} TO: {dst}')
        #print('FROM: '+src+' TO: '+dst)
        self.details.update({
            'src': src,
            'dst': dst
        })

    @extract('/Places/0/PlaceId')
    def get_place(self, query: str) -> list:
        return decode_response(URLS['places'], query=query, **self.details)

    def get_markets(self) -> dict:
        r = decode_response(URLS['markets'], **self.details)
        codes = dpath.values(r, '/Countries/*/Code')
        names = dpath.values(r, '/Countries/*/Name')
        return dict(zip(codes, names))

    @extract('/Quotes/*/MinPrice')
    def get_quotes(self, **kwargs) -> list:
        kwargs.update(self.details)
        return decode_response(URLS['quotes'], **kwargs)

    @duration
    def sweep_cache(self, pool_size: int=10) -> list:
        markets = self.get_markets()
        # make parallel queries for each market
        pool = ProcessingPool(pool_size)
        def get_quotes_(m: str) -> dict:
            quotes = self.get_quotes(country=m)
            return {m: quotes}
        # combine results
        res = {}
        for _ in pool.map(get_quotes_, markets.keys()):
            res.update(_)
        # sort
        res = [(m, markets[m], quotes) for m, quotes in res.items()]
        return sorted(res, key=lambda t: (t[2], t[1]))

    def get_session(self, country: str) -> str:
        r = post_response(country=country, **self.details)
        try:
            session = dpath.get(r, '/Location')
        except:
            print('R:', r)
        time.sleep(.1)
        session = session.rstrip('/')
        return f'{session}?apikey={KEY}'
        #return session+'?apikey='+KEY

    @duration
    def sweep_real_time(self, req_id: int, encoding: str='utf-8') -> list:
        global reqs 
        print(f'> Starting to sweep markets for request: {req_id}')
        #print('> Starting to sweep markets for request: '+req_id)
        markets = self.get_markets()
        for m in markets:
            url_to_poll = self.get_session(m)
            bstr = requests.get(url_to_poll).content
            r = json.loads(bstr.decode(encoding))
            prices = dpath.values(r, '/Itineraries/*/PricingOptions/*/Price')
            urls = dpath.values(r, '/Itineraries/*/PricingOptions/*/DeeplinkUrl')
            # retrieve best price per market
            sorted_res = sorted(zip(prices, urls))
            prices, urls = zip(*sorted_res)
            if prices:
                entry  = (m, markets[m], prices[0], urls[0])
                # print(*entry)
                # update reqs -> update file
                reqs[req_id].table.append(entry)
        # mark job completion
        reqs[req_id].poll_id = 0

    @extract('/Locales/*/Code')
    def get_locales(self) -> list:
        return decode_response(URLS['locales'])

    @extract('/Currencies/*/Code')
    def get_currencies(self) -> list:
        return decode_response(URLS['currencies'])


# ------------------------------------------------------------------------------
# ---------------------------------- SERVER ------------------------------------
# ------------------------------------------------------------------------------
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/getlive/<src>/<dst>/<date_out>')
def sweep_real_time(src, dst, date_out):
    # allocate id
    req_id = max(reqs.keys()) + 1
    reqs[req_id] = Request(req_id, src, dst, date_out)
    print(f'> Allocated request id: {req_id}')
    #print('> Allocated request id: '+req_id)
    # start a new thread to sweep markets for request id req_id
    sweep_markets = MarketsSweeper(src, dst, date_out).sweep_real_time
    Thread(target=sweep_markets, args=(req_id,)).start()
    return get_req(req_id)


@app.route('/req_id/<req_id>')
def get_req(req_id):
    req_id = int(req_id)
    pprint_reqs()
    r = reqs[req_id]
    table = sorted(r.table, key=lambda t: (t[2], t[1]))
    return render_template('loading.html', sofars=table, poll_id=r.poll_id, src=r.src, dst=r.dst, date_out=r.date_out)

@app.route('/req_id_clicked/<req_id>')
def get_req_paused(req_id):
    req_id = int(req_id)
    pprint_reqs()
    r = reqs[req_id]
    table = sorted(r.table, key=lambda t: (t[2], t[1]))
    return render_template('loading-clicked.html', sofars=table, poll_id=r.poll_id, src=r.src, dst=r.dst, date_out=r.date_out)


@app.route('/getprice/<src>/<dst>/<date_out>')
def sweep_cache(src, dst, date_out):
    return render_template('index-table.html', res=MarketsSweeper(src, dst, date_out).sweep_cache())


if __name__ == '__main__':


    # Create a command line argument parser
    parser = argparse.ArgumentParser(description='JobAllocator. Manage task allocation for asynchronous distributed tasks.')
    # Add a jobid argument
    parser.add_argument('--flaskport', action="store", default='5000', help="ID of the currently running JOB")  
    # Parse command line args
    clargs = parser.parse_args()
    # Access command line argument
    input_port = clargs.flaskport


    app.run(host='localhost', port=input_port, debug=True)
