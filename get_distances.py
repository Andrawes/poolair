import requests
import main
import json
from lxml import html

URL = 'https://www.distance.to/%(src)s/%(dst)s'
SOURCES = ['Atlanta', 'Chicago', 'London', 'Paris', 'Tokyo', 'Los Angeles', 'Dubai', 'Beijing', 'Hong Kong', 'Zurich']

def get_countries():
    markets = main.MarketsSweeper('London', 'London', '').get_markets()
    countries = [(mname, mcode) for mcode, mname in markets.items()]
    return sorted(countries)

def get_distances():
    countries = get_countries()
    res_name = {}
    res_code = {}
    for s in SOURCES:
        print(f'> Source: {s}')
        res_name[s] = {}
        res_code[s] = {}
        for (cname, ccode) in countries:
            page = requests.get(URL % {'src': s, 'dst': cname})
            tree = html.fromstring(page.content)
            # get distance in km from website
            header = tree.xpath('//span[@class="headerAirline"]')[0]
            distance = header.xpath('//span[@class="value km"]/text()')[0]
            distance = distance.replace(',', '')
            distance = float(distance)
            print(f'  - Dest: {ccode} - {cname}')
            print(f'      {distance} km')
            res_name[s][cname] = distance
            res_code[s][ccode] = distance
    with open('distances_names.json', 'wt') as f:
        f.write(json.dumps(res_name, indent=4))
    with open('distances_codes.json', 'wt') as f:
        f.write(json.dumps(res_code, indent=4))

if __name__ == '__main__':
    get_distances()
