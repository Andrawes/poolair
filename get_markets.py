import main

markets = main.MarketsSweeper('London', 'London', '').get_markets()
with open('countries.list', 'wt') as f:
    countries = [mname for mcode, mname in markets.items()]
    countries.sort()
    for c in countries:
        f.write(c + '\n')
